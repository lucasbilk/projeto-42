/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lucas
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse elipse1 = new Elipse(10,10);
        Circulo circulo1 = new Circulo(10);
        
        System.out.println(elipse1.getArea());
        System.out.println(elipse1.getPerimetro());
        
        System.out.println(circulo1.getArea());
        System.out.println(circulo1.getPerimetro());      
    }
}
